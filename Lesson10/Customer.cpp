#pragma once
#include "Customer.h"


Customer::Customer(string name)
{
	this->_name = name;
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	double res = 0;
	for (set<Item>::iterator it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		res += (*it).totalPrice();
	}
	return res;
}

void Customer::addItem(Item i)
{
	if (!this->CheckItemExist(i.GetName()))
	{
		this->_items.insert(i);
	}
	else
	{
		for (set<Item>::iterator it = this->_items.begin(); it != this->_items.end(); it++)
		{
			if ((*it).GetName() == i.GetName())
			{
				((Item&)(*it)).SetCount((*it).GetCount() + 1);
			}
		}
	}
}

void Customer::removeItem(Item i)
{
	for (set<Item>::iterator it = this->_items.begin(); it != this->_items.end(); it++)
	{
		if ((*it).GetName() == i.GetName())
		{
			if ((*it).GetCount() > 1)
			{
				((Item&)(*it)).SetCount((*it).GetCount() - 1);
			}
			else
			{
				this->_items.erase(i);
			}
			return;
		}
	}
}

void Customer::SetName(string name)
{
	this->_name = name;
}

string Customer::GetName()
{
	return this->_name;
}

set<Item>& Customer::GetItems()
{
	return this->_items;
}

bool Customer::CheckItemExist(string name)
{
	for (set<Item>::iterator it = this->_items.begin(); it != this->_items.end(); it++)
	{
		if ((*it).GetName() == name)
		{
			return true;
		}
	}

	return false;
}